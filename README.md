# ZenbookS13OLED

My ZenbookS13 OLED

## Reference: UM5302T

## VRAM Unlock

Note: I reverted the changes, as I didn't see any difference in performance (need to go further in the tests). Now everything is back to default, (Auto).

I tried to follow this guide: https://winstonhyypia.medium.com/amd-apu-how-to-modify-the-dedicated-gpu-memory-e27b75905056

The default amount of Video-RAM seems limited with the default configuration:
* 512MB In productivity mode
* 2GB In Game mode (Option in AMD Adrenalyn)

## Universal AMD Form Browser

BIOS doesn't have the option to change this, but there is this little application that can boot (FreeDOS) and give access to an extra more advanced bios-like menu.

https://github.com/DavidS95/Smokeless_UMAF

Here, the app is provided for backup purposes:

[Universal AMD Form Browser](./UniversalAMDFormBrowser.zip)

## Procedure

Here is a backup copy of the procedure I followed (medium article):

* Format a USB drive to FAT32, FreeDOS (with Rufus). Copy-paste the files from the zip to the USB drive.
* Then boot from the USB drive (ESC, boot menu, select the USB drive).

* Then, follow the procedure bellow :

```
Device Manager > AMD CBS > NBIO Common Options > GFX Configuration

iGPU Configuration = <UMA_SPECIFIED>
UMA Version = <Auto>
UMA Frame Buffer Size = <8G>
GPU Host Translation Chace = <Auto>
```
